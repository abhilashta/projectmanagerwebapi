﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ProjectManagerWebAPI;
using ProjectManagerEntitiesLib;
using ProjectManagerWebAPI.Controllers;

namespace TestWebAPI
{
    public class TestWebAPI
    {
        [Test]
        public void TestWebAPIUserGetAll()
        {
            ProjectManagerController obj = new ProjectManagerController();
            int actual = obj.GetAllUser().Count;
            Assert.Greater(actual, 0);
        }
        [Test]
        public void TestWebAPIAddUser()
        {
            ProjectManagerController obj = new ProjectManagerController();
            User item = new User();
            item.FirstName = "Jeff";
            item.LastName = "Rose";
            item.EmployeeId = 56435;
            obj.PostUser(item);
            int actual = obj.GetAllUser().Count;
            Assert.Greater(actual, 2);

        }
        [Test]
        public void TestWebAPIParentTaskGetAll()
        {
            ProjectManagerController obj = new ProjectManagerController();
            int actual = obj.GetAllParentTask().Count;
            Assert.Greater(actual, 0);
        }
        //[Test]
        //public void TestWebAPIAddParentTask()
        //{
        //    ProjectManagerController obj = new ProjectManagerController();
        //    ParentTask item = new ParentTask();         
        //    item.Parent_Task = "ParentTaskTestWebAPI";
        //    obj.PostParentTask(item);
        //    int actual = obj.GetAllParentTask().Count;
        //    Assert.Greater(actual, 1);

        //}
        [Test]
        public void TestWebAPIProjectGetAll()
        {
            ProjectManagerController obj = new ProjectManagerController();
            int actual = obj.GetAllProject().Count;
            Assert.Greater(actual, 0);
        }
        [Test]
        public void TestWebAPIAddProject()
        {
            ProjectManagerController obj = new ProjectManagerController();
            Project item = new Project();
            item.Project_Name = "TestProject2";
            item.Start_Date = System.DateTime.Now;
            item.End_Date = System.DateTime.Now.AddDays(1);
            item.Priority = 12;
            item.Manager_ID = 2;
            obj.PostProject(item);
            int actual = obj.GetAllProject().Count;
            Assert.Greater(actual, 1);

        }
        [Test]
        public void TestWebAPIUpdateProject()
        {
            ProjectManagerController obj = new ProjectManagerController();
            Project item = new Project();
            item.Project_Name = "TestProject";
            item.Start_Date = System.DateTime.Now;
            item.End_Date = System.DateTime.Now.AddDays(1);
            item.Priority = 15;
            item.Manager_ID = 2;
            obj.PostProject(item);
            int actual = obj.GetAllParentTask().Count;
            Assert.Greater(actual, 1);

        }
        [Test]
        public void TestWebAPIUpdateUser()
        {
            ProjectManagerController obj = new ProjectManagerController();
            User item = new User();
            item.User_ID = 2;
            item.FirstName = "John";
            item.LastName = "Doe";
            item.EmployeeId = 1000;
            obj.PutUser(item);
            int actual = obj.GetAllUser().Count;
            Assert.Greater(actual, 1);

        }

        [Test]
        public void TestWebAPIGetAll()
        {
            ProjectManagerController obj = new ProjectManagerController();
            int actual = obj.GetAllTask().Count;
            Assert.Greater(actual, 0);
        }
        [Test]
        public void TestWebAPIGetByTaskId()
        {
            ProjectManagerController obj = new ProjectManagerController();
            Task item = obj.GetByTaskId(2);
            Assert.AreEqual(2, item.Task_ID);
        }
        [Test]
        public void TestWebAPIAddTask()
        {
            ProjectManagerController obj = new ProjectManagerController();
            Task item = new Task();
            item.TaskName = "UnitTest10";
            item.Priority = 15;
            item.Start_Date = System.DateTime.Now;
            item.End_Date = System.DateTime.Now;
            item.Status = 0;
            item.Project_ID = 3;
            item.TASK_OWNER_ID = 2;
            obj.PostTask(item);
            Task test = obj.GetByTaskName("UnitTest10");
            Assert.AreEqual("UnitTest10", test.TaskName);

        }
        //[Test]
        //public void TestDeleteTask()
        //{

        //    ProjectManagerBusiness obj = new ProjectManagerBusiness();
        //    obj.DeleteTask(6024);
        //    Task item = obj.GetByTaskId(6024);
        //    Assert.AreEqual(null, item);
        //}
        [Test]
        public void TestWebAPIUpdateTask()
        {
            ProjectManagerController obj = new ProjectManagerController();
            Task item = new Task();
            item.Task_ID = 2;
            item.TaskName = "TestProjectTask1";
            item.Priority = 18;
            item.Start_Date = System.DateTime.Now;
            item.End_Date = System.DateTime.Now;
            item.Project_ID = 3;
            item.TASK_OWNER_ID = 2;
            item.Status = 0;
            obj.PutTask(item);
            Task itemafterupdate = obj.GetByTaskName("TestProjectTask1");
            Assert.AreEqual(18, itemafterupdate.Priority);

        }
        [Test]
        public void TestWebAPIUpdateEndTask()
        {
            ProjectManagerController obj = new ProjectManagerController();
            Task item = new Task();
            item.Task_ID = 2;
            item.TaskName = "TestProjectTask1";
            item.Priority = 18;
            item.Start_Date = System.DateTime.Now;
            item.End_Date = System.DateTime.Now;
            item.Project_ID = 3;
            item.TASK_OWNER_ID = 2;
            item.Status = 0;
            obj.UpdateEndDate(item);
            Task itemafterupdate = obj.GetByTaskName("TestProjectTask1");
            Assert.AreEqual(1, itemafterupdate.Status);


        }
    }
}
